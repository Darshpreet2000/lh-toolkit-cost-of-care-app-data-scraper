import datetime
import importlib
import os
import sys
import json
import pandas


sys.path.append(os.getcwd())

from ParseData import ParseData
a = ParseData()
here = os.path.dirname(os.path.abspath(__file__))
folder = os.path.basename(here)

basePath=here.split("process_cdm")[0]
destination=os.path.join(basePath,"CDM")
pathToData=os.path.join(basePath,'Data')
here=os.path.join(os.path.join(pathToData,'Georgia'),folder)
if not os.path.exists(here):
    print("No Data Found For {0}".format(here))
    exit(0)
if not os.path.exists(destination):
    os.mkdir(destination)
print(destination)
state = os.path.join(destination, 'Georgia')  # Storing Statewise
if not os.path.exists(state):
    os.mkdir(state)
destination = os.path.join(state, folder+".csv")
if os.path.exists(destination):
    os.remove(destination)     #Removing from CDM
for filename in os.listdir(here):
   if '.txt' in filename:
    name = folder
    filename = os.path.join(here, filename)
    if not os.path.exists(filename):
        print('%s is not found in folder.' % filename)
        continue
    n = 2
    category = "Standard"
    description='Procedure Description'
    charge= 'Charge'
    a.ProcessCSV(n, filename,  name, charge, description, category, destination)
