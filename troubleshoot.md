## TroubleShooting

**You can create an issues here, we will try to reply as soon as possible**

##### XVFB Error
```
raise EasyProcessError(self, "start error")
easyprocess.EasyProcessError: start error <EasyProcess cmd_param=['Xvfb', '-help'] cmd=['Xvfb', '-help'] oserror=[WinError 2] The sy
stem cannot find the file specified return_code=None stdout="None" stderr="None" timeout_happened=False>
```

You may forget to remove the following Code from scrapCDM/middlewares.py file

```python
display = Display(visible=0, size=(800, 800))
display.start()
```
This is used in CI Pipeline, because there is no display output for the browser to launch in. 

##### ChromeDriver & Browser Exception

###### SessionNotCreatedException 
```
SessionNotCreatedException: session not created: This version of ChromeDriver only supports Chrome version 83 using Selenium ChromeDriver
```
Make sure chromedriver & chromebrowser have same version


###### WebDriverException
```
Selenium: WebDriverException:Chrome failed to start: crashed as google-chrome is no longer running so ChromeDriver is assuming
that Chrome has crashed
```
**Reason and Solutions**

The pottential reasons and solutions are:

- Chrome is not at all installed within the system, so you have to install Chrome
- Chrome is not installed at the default location, so you have to pass the correct location of chrome executable through binary_location property.
- The symlink /usr/bin/google-chrome to the actual Chrome binary got corrupted, so you may have to create the symlink.
- The user doesn't have required access rights /usr/bin/google-chrome, so you have provide the access rights.